import cv2
import numpy as np
# from matplotlib import pyplot as plt

categories = [
    "Speed limit 20 (prohibitory)",
    "Speed limit 30 (prohibitory)",
    "Speed limit 50 (prohibitory)",
    "Speed limit 60 (prohibitory)",
    "Speed limit 70 (prohibitory)",
    "Speed limit 80 (prohibitory)",
    "Restriction ends 80 (other)",
    "Speed limit 100 (prohibitory)",
    "Speed limit 120 (prohibitory)",
    "No overtaking (prohibitory)",
    "No overtaking (trucks) (prohibitory)",
    "Priority at next intersection (danger)",
    "Priority road (other)",
    "Give way (other)",
    "Stop (other)",
    "No traffic both ways (prohibitory)",
    "No trucks (prohibitory)",
    "No entry (other)",
    "Danger (danger)",
    "Bend left (danger)",
    "Bend right (danger)",
    "Bend (danger)",
    "Uneven road (danger)",
    "Slippery road (danger)",
    "Road narrows (danger)",
    "Construction (danger)",
    "Traffic signal (danger)",
    "Pedestrian crossing (danger)",
    "School crossing (danger)",
    "Cycles crossing (danger)",
    "Snow (danger)",
    "Animals (danger)",
    "Restriction ends (other)",
    "Go right (mandatory)",
    "Go left (mandatory)",
    "Go straight (mandatory)",
    "Go right or straight (mandatory)",
    "Go left or straight (mandatory)",
    "Keep right (mandatory)",
    "Keep left (mandatory)",
    "Roundabout (mandatory)",
    "Restriction ends (overtaking) (other)",
    "Restriction ends (overtaking (trucks)) (other)"
]
font = cv2.FONT_HERSHEY_SIMPLEX

def box(img,x,y,w,h,text):
    cv2.putText(img, text ,(x,y-25), font, 0.5,(105,62,80),1,cv2.LINE_AA)
    cv2.rectangle(img, (x, y), (x + w, y + h), (88, 195, 0), 1)

def description(f,x,y,w,h,cat):
    print "%05d.ppm;%d;%d;%d;%d;%02d" % (f,x,y,x+w,y+h,cat)

i = 0
for i in range(100):
    founds = 0
    img = cv2.imread("./test-images/TestIJCNN2013/%05d.ppm" % (i, ))
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    for cat in range(2): # 42 in future
        cname = "./haar-training/%02d/classifier/cascade.xml" % cat
        cascade = cv2.CascadeClassifier(cname)
        
        detections = cascade.detectMultiScale(gray,1.1,2,0,(10, 10),(100, 100))
#         detection = cascade.detectMultiScale(gray)
        for (x, y, w, h) in detections:
            box(img,x,y,w,h,categories[cat])
            founds += 1
            roi_gray = gray[y:y+h, x:x+w]
            roi_color = frame[y:y+h, x:x+w]
            checkit = cascade.detectMultiScale(roi_gray)
            for (ex, ey, ew, eh) in checkit:
                cv2.rectangle(roi_color, (ex, ey), (ex+ew, ey+eh), (0,255,0), 2)
                description(i,x,y,w,h,cat)

        
    cv2.imshow('img', img)
    cv2.waitKey()
    
    if founds:
        fname = "./test-founds/%05d.jpg" % i;
        img = cv2.resize(img, (800, 600))
        cv2.imwrite(fname, img)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    i += 1
    if i > 100:
        break

cv2.destroyAllWindows()
