import cv2
import numpy as np
# from matplotlib import pyplot as plt

car_cascade = cv2.CascadeClassifier('Haarcascades/haarcascade_car.xml')
spede_cascade = cv2.CascadeClassifier('Haarcascades/haarcascade_car.xml')
stop_sign_cascade = cv2.CascadeClassifier('Haar-Cascade-Classifiers/frontal_stop_sign_cascade.xml')

i = 0
for i in range(100):
    img = cv2.imread("./FullIJCNN2013/%05d.ppm" % (i, ))
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cars = car_cascade.detectMultiScale(gray)
    for (x, y, w, h) in cars:
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
        # roi_gray = gray[y:y + h, x:x + w]
        # roi_color = img[y:y + h, x:x + w]
        # eyes = eye_cascade.detectMultiScale(roi_gray)
        # for (ex, ey, ew, eh) in eyes:
        #     cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)

    stops = stop_sign_cascade.detectMultiScale(gray)
    for (x, y, w, h) in stops:
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 255), 2)
        # roi_gray = gray[y:y + h, x:x + w]
        # roi_color = img[y:y + h, x:x + w]
        # eyes = eye_cascade.detectMultiScale(roi_gray)
        # for (ex, ey, ew, eh) in eyes:
        #     cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)

    cv2.imshow('img', img)
    cv2.waitKey()

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    i += 1
    if i > 100:
        break

cv2.destroyAllWindows()
