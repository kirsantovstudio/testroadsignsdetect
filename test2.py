import cv2
import numpy as np
# from matplotlib import pyplot as plt

categories = [
    "Speed limit 20 (prohibitory)",
    "Speed limit 30 (prohibitory)",
    "Speed limit 50 (prohibitory)",
    "Speed limit 60 (prohibitory)",
    "Speed limit 70 (prohibitory)",
    "Speed limit 80 (prohibitory)",
    "Restriction ends 80 (other)",
    "Speed limit 100 (prohibitory)",
    "Speed limit 120 (prohibitory)",
    "No overtaking (prohibitory)",
    "No overtaking (trucks) (prohibitory)",
    "Priority at next intersection (danger)",
    "Priority road (other)",
    "Give way (other)",
    "Stop (other)",
    "No traffic both ways (prohibitory)",
    "No trucks (prohibitory)",
    "No entry (other)",
    "Danger (danger)",
    "Bend left (danger)",
    "Bend right (danger)",
    "Bend (danger)",
    "Uneven road (danger)",
    "Slippery road (danger)",
    "Road narrows (danger)",
    "Construction (danger)",
    "Traffic signal (danger)",
    "Pedestrian crossing (danger)",
    "School crossing (danger)",
    "Cycles crossing (danger)",
    "Snow (danger)",
    "Animals (danger)",
    "Restriction ends (other)",
    "Go right (mandatory)",
    "Go left (mandatory)",
    "Go straight (mandatory)",
    "Go right or straight (mandatory)",
    "Go left or straight (mandatory)",
    "Keep right (mandatory)",
    "Keep left (mandatory)",
    "Roundabout (mandatory)",
    "Restriction ends (overtaking) (other)",
    "Restriction ends (overtaking (trucks)) (other)"
]
font = cv2.FONT_HERSHEY_SIMPLEX
descriptions = []

def box(img,x,y,w,h,text,col,t):
    cv2.putText(img, text ,(x,y-25), font, 0.5,col,t,cv2.LINE_AA)
    cv2.rectangle(img, (x, y), (x + w, y + h), col, t)

def description(f,x,y,w,h,cat):
    res = "%05d.ppm;%d;%d;%d;%d;%02d" % (f,x,y,x+w,y+h,cat)
    print res
    descriptions.append(res)
    
def savereport():
    res = open("test-results/testIJCNN2013.txt", "w")
#     res.dump(mylist, myfile)
    for line in descriptions:
        res.write("%s\n" % line)
    res.close()

i = 0
for i in range(300):
    founds = 0
    img = cv2.imread("./test-images/TestIJCNN2013/%05d.ppm" % (i, ))
    img = cv2.imread("./test-images/FullIJCNN2013/%05d.ppm" % (i, ))
    img = cv2.medianBlur(img,1)
    ret,img = cv2.threshold(img,0,200,3)
#     img = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    for cat in range(12): # 42 in future ,1,2,4,5,7,8,9
        if cat==0 or cat==3 or cat==6:
            continue;
        cname = "./haar-training/%02d/classifier/cascade.xml" % cat
        cascade = cv2.CascadeClassifier(cname)
        
        detections = cascade.detectMultiScale(gray,1.3,6,0,(5, 5),(140, 140))
#         print detections
#         detection = cascade.detectMultiScale(gray)
        for (x, y, w, h) in detections:
            box(img,x,y,w,h,categories[cat],(88, 195, 0), 1)
            founds += 1
            roi_gray = gray[y:y+h, x:x+w]
            roi_color = img[y:y+h, x:x+w]
            checkit = cascade.detectMultiScale(roi_gray)
            description(i,x,y,w,h,cat)
            for (ex, ey, ew, eh) in checkit:
                box(roi_color, ex, ey, ex+ew, ey+eh,'', (0,255,0), 2)
        
    cv2.imshow('img', img)
    cv2.waitKey()
    
    if founds:
        fname = "./test-founds/%05d.jpg" % i;
        img = cv2.resize(img, (800, 600))
        cv2.imwrite(fname, img)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    i += 1

savereport()
cv2.destroyAllWindows()
