Размещаем проект на сервере Ubuntu 16.04

cd ~

sudo apt-get update

sudo apt-get upgrade


Создаем рабочее пространство:

mkdir opencv_workspace

cd opencv_workspace


Размещаем репозиторий OpenCV:

sudo apt-get install git

git clone https://github.com/opencv/opencv.git


Устанавливаем необходимые библиотеки и компиляторы:

Compiler: sudo apt-get install build-essential

Libraries: sudo apt-get install cmake libgtk2.0-dev libavcodec-dev libavformat-dev libswscale-dev

Python bindings and such: sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev

Устанавливаем библиотеку OpenCV:

sudo apt-get install libopencv-dev


