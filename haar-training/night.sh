#!/bin/bash
for i in `seq $1 $2`; do
	foo=$(printf "%02d" $i)
	echo Start training "${foo}"
	bash train.sh "${foo}" 20
done
