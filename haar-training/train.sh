# mmm
echo "Use bash train.sh dir numStages"
echo "Start preparing for training $1 classifier"
cd $1
mkdir positives
mkdir classifier
mv *.ppm ./positives
cd ..

find $1/positives -iname "*.ppm" > positives.txt 

echo "folder prepered."

echo "Start sampling..."
rm -rf samples
mkdir samples
perl tools/createsamples.pl positives.txt negatives.txt samples 1500 "opencv_createsamples -bgcolor 0 -bgthresh 0 -maxxangle 1.1 -maxyangle 1.1 maxzangle 0.5 -maxidev 40 -w 24 -h 24"

python tools/mergevec.py -v samples -o samples.vec
echo "ready to training >>>"

opencv_traincascade -data $1/classifier -vec samples.vec -bg negatives.txt\
          -numStages $2 -minHitRate 0.999 -maxFalseAlarmRate 0.5 -numPos 1000\
          -numNeg 1000 -w 24 -h 24 -mode ALL -precalcValBufSize 1024\
          -precalcIdxBufSize 1024

echo "Ready!!!"
rm -rf samples