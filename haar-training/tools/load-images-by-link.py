import urllib
import urllib2
import os
import cv2
import numpy as np

def store_raw_images():
    neg_images_link = 'http://image-net.org/api/text/imagenet.synset.geturls?wnid=n00017222'   
    # neg_images_link = 'http://image-net.org/api/text/imagenet.synset.geturls?wnid=n07942152'   
    # neg_images_link = 'http://image-net.org/api/text/imagenet.synset.geturls?wnid=n00523513'   

    neg_image_urls = urllib2.urlopen(neg_images_link).read().decode()
    pic_num = 1052 # for append images change after saving number next last
    
    if not os.path.exists('negatives'):
        os.makedirs('negatives')
        
    for i in neg_image_urls.split('\n'):
        # if pic_num < 15: 
        #     pic_num += 1
        #     continue;
        try:
            print(i)
            urllib.urlretrieve(i, "negatives/"+str(pic_num)+".jpg")
            img = cv2.imread("negatives/"+str(pic_num)+".jpg",cv2.IMREAD_GRAYSCALE)
            # should be larger than samples / pos pic (so we can place our image on it)
            resized_image = cv2.resize(img, (100, 100))
            cv2.imwrite("negatives/"+str(pic_num)+".jpg",resized_image)
            pic_num += 1
            
        except Exception as e:
            print(str(e)) 

def find_uglies():
    match = False
    for file_type in ['negatives']:
        for img in os.listdir(file_type):
            for ugly in os.listdir('uglies'):
                try:
                    current_image_path = str(file_type)+'/'+str(img)
                    ugly = cv2.imread('uglies/'+str(ugly))
                    question = cv2.imread(current_image_path)
                    if ugly.shape == question.shape and not(np.bitwise_xor(ugly,question).any()):
                        print('That is one ugly pic! Deleting!')
                        print(current_image_path)
                        os.remove(current_image_path)
                except Exception as e:
                    print(str(e))

# store_raw_images()
find_uglies()